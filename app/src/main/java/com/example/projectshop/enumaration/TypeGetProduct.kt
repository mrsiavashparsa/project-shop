package com.example.projectshop.enumaration

import java.io.Serializable

enum class TypeGetProduct :Serializable { NEW_PRODUCT,DISCOUNT_PRODUCT,TOP_SELLING_PRODUCT }