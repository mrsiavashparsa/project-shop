package com.example.projectshop.utility

import android.widget.ImageView
import com.example.projectshop.R
import com.squareup.picasso.Picasso
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PicassoUtility:KoinComponent {

private val picasso:Picasso by inject()

    fun setImage(address:String,img:ImageView){

        picasso
            .load(address)
            .placeholder(R.drawable.place_holder)
            .error(R.drawable.error)
            .fit()
            .into(img)
    }


}