package com.example.projectshop.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projectshop.R
import com.example.projectshop.adapter.RecyclerCategoryAdapter
import com.example.projectshop.dataClass.DataCategory
import com.example.projectshop.dataClass.DataImageUrl
import com.example.projectshop.dataClass.DataProduct
import com.example.projectshop.enumaration.TypeGetProduct
import com.example.projectshop.presenter.PresenterHomeFragment
import com.example.projectshop.utility.PicassoUtility
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject

class
HomeFragment : Fragment() {

    private val presenter: PresenterHomeFragment by inject()
    private val picasso: PicassoUtility by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recycler_category_home_fragment.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, true)
    }

    override fun onResume() {
        super.onResume()

        presenter.onResume()
    }

    //regionData

    fun setDataNewProductRecycler(data: List<DataProduct>) {

        new_products_home_frag.initRecycler(data, TypeGetProduct.NEW_PRODUCT)

    }

    fun setDiscountProductRecycler(data: List<DataProduct>){

        discount_products_home_frag.initRecycler(data, TypeGetProduct.DISCOUNT_PRODUCT)

    }

    fun setTopSellerProductRecycler(data: List<DataProduct>){

        top_selling_products_home_frag.initRecycler(data, TypeGetProduct.TOP_SELLING_PRODUCT)

    }

    fun setDataRecyclerCategory(data: List<DataCategory>) {

        recycler_category_home_fragment.adapter = RecyclerCategoryAdapter(context, data)
    }

    //endregion

    fun showToast(text:String){


        context?.toast(text)


    }

    fun setImageInBanners(data: DataImageUrl) {

        picasso.setImage(data.image1,img_banner1_home_fragment)
        picasso.setImage(data.image2,img_banner2_home_fragment)


    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()
    }

}