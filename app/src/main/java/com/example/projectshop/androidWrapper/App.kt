package com.example.projectshop.androidWrapper

import android.app.Application
import com.example.projectshop.R
import com.example.projectshop.di.apiModule
import com.example.projectshop.di.fragmentModule
import com.example.projectshop.di.modelModule
import com.example.projectshop.di.presenterModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class App:Application() {

    override fun onCreate() {
        super.onCreate()


        startKoin {

            androidContext(applicationContext)

            modules(listOf(fragmentModule, modelModule, apiModule, presenterModule))

        }


    }


}