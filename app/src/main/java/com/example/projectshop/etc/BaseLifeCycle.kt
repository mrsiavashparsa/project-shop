package com.example.projectshop.etc

interface BaseLifeCycle {

    fun onCreate()

    fun onDestroy()

    fun onResume(){}

    fun onStop(){}

}