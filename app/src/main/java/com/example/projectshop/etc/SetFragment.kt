package com.example.projectshop.etc

import androidx.fragment.app.Fragment

interface SetFragment {

    fun addFragment(fragment: Fragment)

    fun replaceFragment(fragment: Fragment)



}