package com.example.projectshop.view.customView

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projectshop.R
import com.example.projectshop.activity.ArchiveActivity
import com.example.projectshop.adapter.RecyclerItemProductAdapter
import com.example.projectshop.dataClass.DataProduct
import com.example.projectshop.enumaration.TypeGetProduct
import kotlinx.android.synthetic.main.view_product.view.*
import org.jetbrains.anko.startActivity

class ProductView(context: Context, attrs:AttributeSet) : FrameLayout(context,attrs) {

    private val txtTitle:AppCompatTextView
    private val txtAll:AppCompatTextView
    private val recycler:RecyclerView

    companion object{

        const val TITLE_KEY = "title"
        const val TYPE_KEY = "type"


    }


init {

    val mainView= inflate(context, R.layout.view_product,this)

    val typeArray=context.obtainStyledAttributes(attrs,R.styleable.ProductView)
    val text=typeArray.getString(R.styleable.ProductView_titleText)
    typeArray.recycle()


    txtTitle=mainView.txt_product_title
    txtAll=mainView.txt_product_all
    recycler=mainView.recycler_product

    recycler.layoutManager = LinearLayoutManager(context,RecyclerView.HORIZONTAL,true)

    txtTitle.text =text
}

    fun initRecycler(data:List<DataProduct>,type:TypeGetProduct){


        recycler.adapter = RecyclerItemProductAdapter(context,data)

        txtAll.setOnClickListener {

            context.startActivity<ArchiveActivity>(
                TITLE_KEY to txtTitle.text.toString(),
            TYPE_KEY to type)

        }
    }

}