package com.example.projectshop.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.PopupMenu
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import com.example.projectshop.R
import com.example.projectshop.etc.SetFragment
import com.example.projectshop.etc.Utility
import com.example.projectshop.model.ModelMainActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.view.*

@SuppressLint("ViewConstructor")
class ViewMainActivity(contextInstance:Context, private val setFragment:SetFragment,
private val utility: Utility) : FrameLayout(contextInstance),PopupMenu.OnMenuItemClickListener{

    private val bottomNav:BottomNavigationView
    private val imgMenu:AppCompatImageView

    init {

        val view = inflate(context, R.layout.activity_main,this)

        bottomNav=view.bottom_nav_main
        imgMenu=view.menu_main
    }

    fun setUpView(itemChecked:Int,mainFragment:Fragment){

        imgMenu.setOnClickListener {

            utility.onCreatePopOpMenu(imgMenu)

        }



        bottomNav.menu.getItem(itemChecked).isChecked = true

        addFragment(mainFragment)

    }

    fun bottomNavItemClick(fragments:Map<String,Fragment>){


        bottomNav.setOnNavigationItemSelectedListener {

            when(it.itemId){

                R.id.item_menu_account ->{


                    replaceFragment(fragments[ModelMainActivity.KEY_ACCOUNT_FRAGMENT]?:Fragment())
                    true
                }
                R.id.item_menu_home ->{

                    replaceFragment(fragments[ModelMainActivity.KEY_HOME_FRAGMENT]?: Fragment())
                    true
                }
                R.id.item_menu_shop ->{

                    replaceFragment(fragments[ModelMainActivity.KEY_SHOP_FRAGMENT]?:Fragment())
                    true
                }
                else -> false
            }



        }


    }

    private fun addFragment(fragment: Fragment){

        setFragment.addFragment(fragment)
    }

    private fun replaceFragment(fragment: Fragment){

        setFragment.replaceFragment(fragment)

    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {

        return when(item?.itemId){

            R.id.menu_FAQ ->{



                true
            }
            R.id.menu_about_us ->{


                true
            }
            R.id.menu_contact_us ->{


                true
            }
            R.id.menu_failureReport ->{


                true
            }
            else -> false
        }


    }

}