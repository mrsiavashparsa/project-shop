package com.example.projectshop.view

import android.annotation.SuppressLint
import android.content.Context
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projectshop.R
import com.example.projectshop.adapter.RecyclerItemArchiveAdapter
import com.example.projectshop.dataClass.DataProduct
import com.example.projectshop.etc.Utility
import kotlinx.android.synthetic.main.activity_archive.view.*
import org.jetbrains.anko.toast

@SuppressLint("ViewConstructor")
class ViewArchiveActivity(contextInstance:Context, private val utility:Utility): FrameLayout(contextInstance) {

    private val txtTitle:AppCompatTextView
    private val imgBack:AppCompatImageView
    private val recycler:RecyclerView

init {

    val mainView = inflate(context, R.layout.activity_archive,this)

    txtTitle = mainView.txt_title_archive_activity
    imgBack = mainView.img_back_archive_activity
    recycler = mainView.recycler_archive

    recycler.layoutManager = GridLayoutManager(context,2,RecyclerView.VERTICAL,false)

}
    fun setDataInRecycler(data:List<DataProduct>){

        recycler.adapter = RecyclerItemArchiveAdapter(context,data)


    }

    fun setTitleText(title:String){

        txtTitle.text = title

    }



    fun onClickHandler(){

        imgBack.setOnClickListener {

            utility.onFinished()

        }
    }

}