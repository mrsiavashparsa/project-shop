package com.example.projectshop.presenter

import com.example.projectshop.etc.BaseLifeCycle
import com.example.projectshop.model.ModelMainActivity
import com.example.projectshop.view.ViewMainActivity

class PresenterMainActivity(private val view:ViewMainActivity, private val model:ModelMainActivity):BaseLifeCycle {


    override fun onCreate() {

        setUpView()
        setBottomNavItemClick()
    }



    private fun setUpView(){

        view.setUpView(model.getItemChecked(),model.getMainFragment())


    }

    private fun setBottomNavItemClick(){

        view.bottomNavItemClick(model.getAllFragments())


    }

    override fun onDestroy() {


    }
}