package com.example.projectshop.presenter

import com.example.projectshop.dataClass.DataImageUrl
import com.example.projectshop.etc.BaseLifeCycle
import com.example.projectshop.model.ModelArchiveActivity
import com.example.projectshop.net.CountryPresenterListener

import com.example.projectshop.view.ViewArchiveActivity

class PresenterArchiveActivity(
    private val view: ViewArchiveActivity,
    private val model: ModelArchiveActivity
) :BaseLifeCycle{
    override fun onCreate() {

        onClickListener()
        setDataInRecycler()
        setTitleText()

    }

    private fun onClickListener(){

        view.onClickHandler()

    }

    private fun setDataInRecycler(){

        view.setDataInRecycler(model.getDataInRecycler())

    }

    private fun setTitleText(){

        model.getTitleAsIntent().let { view.setTitleText(it) }

    }


    override fun onDestroy() {

    }

}