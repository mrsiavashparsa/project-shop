package com.example.projectshop.presenter

import com.example.projectshop.dataClass.DataCategory
import com.example.projectshop.dataClass.DataImageUrl
import com.example.projectshop.etc.BaseLifeCycle
import com.example.projectshop.fragment.HomeFragment
import com.example.projectshop.model.ModelHomeFragment
import com.example.projectshop.net.CountryPresenterListener

class PresenterHomeFragment(private val view:HomeFragment,private val model:ModelHomeFragment)
    :BaseLifeCycle  {




    override fun onCreate() {
    }


    override fun onResume() {

        setDataRecycler()
        setUpGetData()
    }

    private fun setDataRecycler(){

        view.setDataNewProductRecycler(model.getDataRecycler())
        view.setDiscountProductRecycler(model.getDataRecycler())
        view.setTopSellerProductRecycler(model.getDataRecycler())
    }

    private fun setUpGetData(){

        model.getImageUrlForBanner(object :CountryPresenterListener<DataImageUrl>{
            override fun onResponse(data: DataImageUrl) {
                view.setImageInBanners(data)
            }

            override fun onFailure(error: String) {
                view.showToast(error)
            }
        })
        model.getDataCategory(object :CountryPresenterListener<List<DataCategory>>{
            override fun onResponse(data: List<DataCategory>) {

                view.setDataRecyclerCategory(data)
            }

            override fun onFailure(error: String) {
                view.showToast(error)
            }


        })
    }

    override fun onDestroy() {

    }

}