package com.example.projectshop.net

import com.example.projectshop.dataClass.DataCategory
import com.example.projectshop.dataClass.DataImageUrl
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class ApiService {

    interface DataApi {

        @GET("89e152c8-9947-40de-bf84-f24e84513571")
        fun getImageUrlForBanner(): Call<DataImageUrl>

        @GET("d201e031-e17d-4432-ac0d-46c7828e4ca9")
        fun getDataCategory(): Call<List<DataCategory>>

    }

    fun getApi(): DataApi =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://run.mocky.io/v3/")
            .build()
            .create(DataApi::class.java)






}