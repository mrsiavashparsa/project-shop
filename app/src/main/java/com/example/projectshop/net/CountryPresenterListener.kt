package com.example.projectshop.net

interface CountryPresenterListener<M> {


    fun onResponse(data:M)

    fun onFailure(error:String)

}