package com.example.projectshop.model

import android.app.Activity
import com.example.projectshop.enumaration.TypeGetProduct
import com.example.projectshop.test.DataTest
import com.example.projectshop.view.customView.ProductView

class ModelArchiveActivity(private val activity: Activity) {


    fun getDataInRecycler()=DataTest.getDataProduct()

    fun getTitleAsIntent()=activity.intent.getStringExtra(ProductView.TITLE_KEY) ?: ""

    fun getTypeProduct()= activity.intent.getSerializableExtra(ProductView.TYPE_KEY) as TypeGetProduct


}