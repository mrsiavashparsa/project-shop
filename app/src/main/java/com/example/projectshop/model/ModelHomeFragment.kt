package com.example.projectshop.model

import android.util.Log
import com.example.projectshop.dataClass.DataCategory
import com.example.projectshop.dataClass.DataImageUrl
import com.example.projectshop.net.ApiService
import com.example.projectshop.net.CountryPresenterListener
import com.example.projectshop.test.DataTest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelHomeFragment {

    private val apiService = ApiService()

    fun getDataRecycler() = DataTest.getDataProduct()

    fun getDataCategory(mListener: CountryPresenterListener<List<DataCategory>>) {

        apiService.getApi().getDataCategory()
            .enqueue(object :Callback<List<DataCategory>>{
                override fun onResponse(
                    call: Call<List<DataCategory>>,
                    response: Response<List<DataCategory>>
                ) {

                    val data = response.body()

                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("Error Data Null","null_data")

                }

                override fun onFailure(call: Call<List<DataCategory>>, t: Throwable) {

                    Log.e("Error Failure","error in get data : ${t.message}")
                    mListener.onFailure(" خطا در دریافت اطلاعات از سرور")

                }

            })

    }

    fun getImageUrlForBanner(mListener:CountryPresenterListener<DataImageUrl>) {
        apiService.getApi()
            .getImageUrlForBanner()
            .enqueue(object :Callback<DataImageUrl>{
                override fun onResponse(
                    call: Call<DataImageUrl>,
                    response: Response<DataImageUrl>
                ) {

                    val data = response.body()

                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("Error Data Null","null_data")


                }

                override fun onFailure(call: Call<DataImageUrl>, t: Throwable) {

                    Log.e("Error Failure","error in get data : ${t.message}")
                    mListener.onFailure(" خطا در دریافت اطلاعات از سرور")

                }


            })

    }
}