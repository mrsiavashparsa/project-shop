package com.example.projectshop.model

import androidx.fragment.app.Fragment
import com.example.projectshop.fragment.AccountFragment
import com.example.projectshop.fragment.HomeFragment
import com.example.projectshop.fragment.ShopFragment
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ModelMainActivity : KoinComponent {

    private val homeFragment: HomeFragment by inject()
    private val accountFragment: AccountFragment by inject()
    private val shopFragment: ShopFragment by inject()

    companion object{

        const val KEY_HOME_FRAGMENT = "homeFragment"
        const val KEY_ACCOUNT_FRAGMENT = "accountFragment"
        const val KEY_SHOP_FRAGMENT = "shopFragment"


    }



    fun getItemChecked() = 1

    fun getMainFragment() = homeFragment

    fun getAllFragments() = mapOf<String,Fragment>(

        KEY_HOME_FRAGMENT to homeFragment,
        KEY_ACCOUNT_FRAGMENT to accountFragment,
        KEY_SHOP_FRAGMENT to shopFragment



    )


}