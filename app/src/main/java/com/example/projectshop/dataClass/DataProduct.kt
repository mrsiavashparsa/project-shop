package com.example.projectshop.dataClass

data class DataProduct(
    val id:Int ,
    val title:String,
    val imgUrl:String,
    val price :String,
    val discount:Boolean,
    val priceDiscount:String
)