package com.example.projectshop.dataClass

data class DataCategory(
    val id: Int,
    val title: String
)