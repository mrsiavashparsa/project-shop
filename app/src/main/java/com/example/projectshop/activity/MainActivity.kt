package com.example.projectshop.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import com.example.projectshop.R
import com.example.projectshop.etc.SetFragment
import com.example.projectshop.etc.Utility
import com.example.projectshop.model.ModelMainActivity
import com.example.projectshop.presenter.PresenterMainActivity
import com.example.projectshop.view.ViewMainActivity
import org.koin.android.ext.android.inject
import java.text.ParsePosition

class MainActivity : AppCompatActivity(),SetFragment,Utility {

    private val model: ModelMainActivity by inject()
    private lateinit var presenter: PresenterMainActivity
    private lateinit var mainView:ViewMainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainView = ViewMainActivity(this,this,this)
        setContentView(mainView)

        presenter = PresenterMainActivity(mainView,model)
        presenter.onCreate()


    }

    override fun onCreatePopOpMenu(view: View) {

        val popUp=PopupMenu(this,view)
        popUp.menuInflater.inflate(R.menu.menu_main,popUp.menu)
        popUp.setOnMenuItemClickListener(mainView)
        popUp.show()

    }


    override fun addFragment(fragment: Fragment) {

        supportFragmentManager.beginTransaction()
            .add(R.id.main_frame,fragment)
            .commit()
    }

    override fun replaceFragment(fragment: Fragment) {

        supportFragmentManager.beginTransaction()
            .replace(R.id.main_frame,fragment)
            .commit()

    }
    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()
    }
}