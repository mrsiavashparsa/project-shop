package com.example.projectshop.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.projectshop.R
import com.example.projectshop.etc.Utility
import com.example.projectshop.model.ModelArchiveActivity
import com.example.projectshop.presenter.PresenterArchiveActivity
import com.example.projectshop.view.ViewArchiveActivity
import org.koin.android.ext.android.inject

class ArchiveActivity : AppCompatActivity(), Utility {

    private lateinit var presenter:PresenterArchiveActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewArchiveActivity(this,this)
        setContentView(view)

        val model = ModelArchiveActivity(this)

        presenter = PresenterArchiveActivity(view,model)
        presenter.onCreate()

    }

    override fun onFinished() {

        finish()
    }



    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()
    }


}