package com.example.projectshop.di


import com.example.projectshop.fragment.AccountFragment
import com.example.projectshop.fragment.HomeFragment
import com.example.projectshop.fragment.ShopFragment
import com.example.projectshop.model.ModelArchiveActivity
import com.example.projectshop.model.ModelHomeFragment
import com.example.projectshop.model.ModelMainActivity
import com.example.projectshop.presenter.PresenterHomeFragment
import com.example.projectshop.utility.PicassoUtility
import com.squareup.picasso.Picasso
import org.koin.dsl.module

val fragmentModule = module {

    single { HomeFragment() }
    single { AccountFragment() }
    single { ShopFragment() }


}

val modelModule = module {

    single { ModelMainActivity() }
    single { ModelHomeFragment() }


}

val apiModule = module {

    single { Picasso.get() }
    single { PicassoUtility() }

}

val presenterModule = module {

    single { PresenterHomeFragment(get() as HomeFragment, get() as ModelHomeFragment) }

}