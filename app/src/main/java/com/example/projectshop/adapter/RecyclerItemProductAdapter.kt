package com.example.projectshop.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projectshop.R
import com.example.projectshop.dataClass.DataProduct
import com.example.projectshop.utility.PicassoUtility
import kotlinx.android.synthetic.main.item_recycler_product_view.view.*
import org.jetbrains.anko.toast
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class RecyclerItemProductAdapter(private val context: Context, private val data: List<DataProduct>) :
    RecyclerView.Adapter<RecyclerItemProductAdapter.ItemProductViewHolder>() {

    inner class ItemProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        KoinComponent {

        private val picasso: PicassoUtility by inject()
        private val rootView = itemView.constraint_recycler_product_view
        private val img = itemView.img_recycler_product_view
        private val title = itemView.txt_titile_recycler_product_view
        private val price = itemView.txt_price_recycler_product_view
        private val discount = itemView.customTextView_discount


        fun setData(data: DataProduct) {

            title.text = data.title
            picasso.setImage(data.imgUrl, img)

            if (data.discount) {

                discount.visibility = View.VISIBLE
                discount.setCustomText(data.price)
                price.text = data.priceDiscount

            } else {

                price.text = data.price
                discount.visibility = View.INVISIBLE

            }

            rootView.setOnClickListener {

                context.toast("onclick")

            }


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemProductViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recycler_product_view, parent, false
            )
        )

    override fun onBindViewHolder(holder: ItemProductViewHolder, position: Int) {

        holder.setData(data[position])

    }

    override fun getItemCount() = data.size


}