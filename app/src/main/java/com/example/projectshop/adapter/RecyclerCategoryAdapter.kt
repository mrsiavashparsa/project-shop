package com.example.projectshop.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projectshop.R
import com.example.projectshop.dataClass.DataCategory
import kotlinx.android.synthetic.main.item_recycler_category.view.*
import org.jetbrains.anko.toast

class RecyclerCategoryAdapter(private val context: Context?, private val data: List<DataCategory>) :
    RecyclerView.Adapter<RecyclerCategoryAdapter.CategoryViewHolder>() {


    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtTitle = itemView.txt_tile_item_category

        fun setData(data: DataCategory) {

            txtTitle.text = data.title

            txtTitle.setOnClickListener {

                context?.toast(data.title)

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=CategoryViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recycler_category,
        parent,false)
    )

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {

        holder.setData(data[position])

    }

    override fun getItemCount()=data.size

}